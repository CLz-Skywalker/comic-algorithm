#!/bin/bash
SELF_DIR=$(cd "$(dirname "$0")"; pwd)
if [ ! $1 ] || [ $1 -lt 0 ] || [ $1 -gt 9 ]
then
    echo "please input vaild test server number[0-9]! (eg: ./sync.sh 0)"
    exit
fi
make clean
make linux
if [ ! -f "./bin/iw-linux-http-server" ] || [ ! -f "./bin/iw-linux-socket-server" ]
then
	echo "make error!"
	exit
fi

SOCKET_PORT=$[ 8001 + $1 * 100 ]
HTTP_PORT=$[ 8000 + $1 * 100 ]

RSYNC_PASSWORD="kl4cde04va1o" rsync -av ${SELF_DIR}/bin/iw-linux-http-server rsync://servertest@10.0.1.72/serverapp/iwv2/bin/iw-linux-http-server-${HTTP_PORT}
RSYNC_PASSWORD="kl4cde04va1o" rsync -av ${SELF_DIR}/bin/iw-linux-socket-server rsync://servertest@10.0.1.72/serverapp/iwv2/bin/iw-linux-socket-server-${SOCKET_PORT}
RSYNC_PASSWORD="kl4cde04va1o" rsync -av ${SELF_DIR}/conf rsync://servertest@10.0.1.72/serverapp/iwv2/

export BECOME_METHOD="sudo -s"
#配置SSH私钥 export PRIVATE_KEY_FILE="~/.ssh/id_rsa"
#配置SSH用户 export REMOTE_USER=jie.zheng
sudo ansible -i ./hosts test -b -m shell -a "supervisorctl restart iw-ws-$SOCKET_PORT iw-http-${HTTP_PORT}" --private-key $PRIVATE_KEY_FILE -u $REMOTE_USER
