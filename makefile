PROJECT="IslandWarServer"
TEST_DIRS=`find . -name "*_test.go" | awk -F '/[^/]*$$' '{print $$1}' | sort -u`
NOW=`date "+%Y-%m-%d %H:%M:%S"`
GO_FILES=`find . -name "*.go" -type f -not -path "./vendor/*"`
PACKAGES=`go list ./... | grep -v /vendor/`
UNFORMATTED=`gofmt -l ./`

default:
	@echo Project: ${PROJECT}

proto:
	@echo "${NOW} Starting to compile proto ..."
	cd ./protocol/define && protoc --go_out=plugins=grpc:./.. *.proto
	@echo "${NOW} Compile proto done!"

fmt:
	@echo "${NOW} Starting to format ..."
	@gofmt -s -w ${GO_FILES}
	@echo "${NOW} Format done!"

fmt-check:
	@echo "${NOW} Starting to format check ..."
	@if [ -z "${UNFORMATTED}" ]; then \
 		echo "${NOW} Format check done!"; \
 	else \
 	  	echo "Below files need to format:"; \
 		echo "${UNFORMATTED}"; \
        exit 1; \
  	fi; \

vet:
	@echo "${NOW} Starting to vet ..."
	@go vet $(PACKAGES)
	@echo "${NOW} Vet done!"

test:
	@echo "${NOW} Starting to run unit test ..."
	go clean -testcache && go test -v ${TEST_DIRS} -gcflags=-l
	@echo "${NOW} Run unit test done!"

test_s:
	@echo "${NOW} Starting to run unit test ..."
	go clean -testcache && go test  ${TEST_DIRS} -gcflags=-l
	@echo "${NOW} Run unit test done!"

mws: default fmt-check vet test_s
	@echo "${NOW} Starting to build WebSocket server for MacOS ..."
	export CGO_ENABLED=0 && export GOOS=darwin && export GOARCH=amd64 && go build -o bin/iw-mac-socket-server app/ws/socketServer.go
	@echo "${NOW} Build done, files in ./bin as follow:"
	@ls -l bin | grep iw

lws: default fmt-check vet test_s
	@echo "${NOW} Starting to build WebSocket server for Linux ..."
	export CGO_ENABLED=0 && export GOOS=linux && export GOARCH=amd64 && go build -ldflags="-s -w" -o bin/iw-linux-socket-server app/ws/socketServer.go
	@echo "${NOW} Build done, files in ./bin as follow:"
	@ls -l bin | grep iw

mhttp: default fmt-check vet test_s
	@echo "${NOW} Starting to build HTTP server for MacOS ..."
	export CGO_ENABLED=0 && export GOOS=darwin && export GOARCH=amd64 && go build -o bin/iw-mac-http-server app/http/httpServer.go
	@echo "${NOW} Build done, files in ./bin as follow:"
	@ls -l bin | grep iw

lhttp: default fmt-check vet test_s
	@echo "${NOW} Starting to build HTTP server for Linux ..."
	export CGO_ENABLED=0 && export GOOS=linux && export GOARCH=amd64 && go build -ldflags="-s -w" -o bin/iw-linux-http-server app/http/httpServer.go
	@echo "${NOW} Build done, files in ./bin as follow:"
	@ls -l bin | grep iw

mac: default fmt-check vet test_s
	@echo "${NOW} Starting to build for MacOS ..."
	export CGO_ENABLED=0 && export GOOS=darwin && export GOARCH=amd64 && go build -o bin/iw-mac-socket-server app/ws/socketServer.go
	export CGO_ENABLED=0 && export GOOS=darwin && export GOARCH=amd64 && go build -o bin/iw-mac-http-server app/http/httpServer.go
	@echo "${NOW} Build done, files in ./bin as follow:"
	@ls -l bin | grep iw

linux: default fmt-check vet test_s
	@echo "${NOW} Starting to build for Linux ..."
	export CGO_ENABLED=0 && export GOOS=linux && export GOARCH=amd64 && go build -ldflags="-s -w" -o bin/iw-linux-socket-server app/ws/socketServer.go
	export CGO_ENABLED=0 && export GOOS=linux && export GOARCH=amd64 && go build -ldflags="-s -w" -o bin/iw-linux-http-server app/http/httpServer.go
	@echo "${NOW} Build done, files in ./bin as follow:"
	@ls -l bin | grep iw

clean:
	@echo "${NOW} Starting to clean ..."
	rm -rf bin/iw-* && rm -rf app.log
	@echo "${NOW} Clean done!"

