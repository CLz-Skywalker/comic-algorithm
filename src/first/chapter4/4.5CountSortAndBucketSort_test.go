package chapter4

import (
	"fmt"
	"sort"
	"testing"
)

//桶排序
func TestBucketSort(t *testing.T) {
	array := []float64{4.12, 6.421, 0.0023, 3.0, 2.123, 8.122, 4.12, 10.09}
	array = bucketSort(array)
	fmt.Println(array)
}

func bucketSort(array []float64) []float64 {
	//1.去除最大值最小值
	var max float64
	var min float64
	for _, v := range array {
		if max < v {
			max = v
		} else if min > v {
			min = v
		}
	}
	d := max - min

	//2.对桶进行初始化
	bucketNum := len(array)
	bucketList := make([][]float64, len(array))

	//3.将元素放入桶内
	for i := 0; i < bucketNum; i++ {
		num := int((array[i] - min) * float64(bucketNum-1) / d)
		if bucketList[num] == nil {
			bucketList[num] = make([]float64, 0)
		}
		bucketList[num] = append(bucketList[num], array[i])
	}

	//4.对桶内数组进行排序
	for i := 0; i < len(bucketList); i++ {
		sort.Float64s(bucketList[i])
	}

	//5.输出到结果数组并返回
	sortedArray := make([]float64, 0, bucketNum)
	for _, float64s := range bucketList {
		for _, v := range float64s {
			sortedArray = append(sortedArray, v)
		}
	}
	return sortedArray
}

//--------------计数排序--------------
func TestCountSort(t *testing.T) {
	array := []uint32{3, 2, 2, 3, 4, 62, 6, 5, 3, 6, 2, 45, 23, 65}
	fmt.Println(countSortV1(array))
}

func countSortV1(list []uint32) []uint32 {
	//1.寻找最大值
	var max uint32
	var min uint32
	length := len(list)
	for i := 0; i < length; i++ {
		if list[i] > max {
			max = list[i]
		} else if list[i] < min {
			min = list[i]
		}
	}
	diff := max - min

	//2.设置线型数组长度
	countArray := make([]uint32, diff+1)

	//3.遍历统计数组，填充统计数组
	for _, value := range list {
		countArray[value-min]++
	}

	//4.遍历统计数组，输出结果
	sortedArray := make([]uint32, 0, length)
	for i, v := range countArray {
		for j := uint32(0); j < v; j++ {
			sortedArray = append(sortedArray, uint32(i)+min)
		}
	}
	return sortedArray
}

//------------------------------------------
