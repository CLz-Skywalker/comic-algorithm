package utils

import "container/list"

//单向链表
type LinkedList struct {
	Value interface{}
	Next  *LinkedList
}

//初始化链表
func (l *LinkedList) Init() *LinkedList {
	l.Next = l
	list.New()
	return l
}

//创建一个指定长度的单向循环链表
func (l LinkedList) New(n int) *LinkedList {
	if n <= 0 {
		return nil
	}
	r := new(LinkedList)
	p := r
	for i := 0; i < n; i++ {
		p.Next = &LinkedList{}
		p = p.Next
	}
	return r
}

//获取下一个节点
func (l *LinkedList) NextNode() *LinkedList {
	if l.Next == nil {
		return nil
	}
	return l.Next
}

//func (l *LinkedList) Prev() *LinkedList {
//	return l.pre
//}
