package stack

import (
	"errors"
	"sync"
)

type ArrayStack struct {
	array []interface{}
	size  int
	lock  sync.RWMutex
}

//入栈
func (stack *ArrayStack) Push(value interface{}) {
	stack.lock.Lock()
	defer stack.lock.Unlock()
	stack.array = append(stack.array, value)
	stack.size += 1
}

//出栈
func (stack *ArrayStack) Pop() (interface{}, error) {
	stack.lock.Lock()
	defer stack.lock.Unlock()
	if stack.size == 0 {
		return nil, errors.New("empty")
	}
	v := stack.array[stack.size-1]
	newArray := make([]interface{}, stack.size-1, stack.size-1)
	for i := 0; i < stack.size-1; i++ {
		newArray[i] = stack.array[i]
	}
	stack.array = newArray
	stack.size--
	return v, nil
}

//获取栈顶元素
func (stack *ArrayStack) Peek() (interface{}, error) {
	stack.lock.RLock()
	defer stack.lock.RUnlock()
	if stack.size == 0 {
		return nil, errors.New("empty")
	}
	return stack.array[stack.size-1], nil
}

//判断栈大小和
func (stack *ArrayStack) Size() int {
	stack.lock.RLock()
	defer stack.lock.RUnlock()
	return stack.size
}

//是否为空
func (stack *ArrayStack) IsEmpty() bool {
	stack.lock.RLock()
	defer stack.lock.RUnlock()
	if stack.size <= 0 {
		return true
	}
	return false
}
