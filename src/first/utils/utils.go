package utils

import (
	"errors"
	"reflect"
	"strconv"
)

/**
 * @Description: 判断数值大小
 * @param condition
 * @param trueVal
 * @param falseVal
 * @return interface{}
 */
func IfNum(condition bool, trueVal, falseVal interface{}) interface{} {
	if condition {
		return trueVal
	} else {
		return falseVal
	}
}

/**
 * @Description: int数组转string
 * @param list
 * @return string
 * @return error
 */
func ArrayToString(list interface{}) (string, error) {
	typeOf := reflect.TypeOf(list).Kind()
	if typeOf == reflect.Array || typeOf == reflect.Slice {
		result := ""
		s := reflect.ValueOf(list)
		for i := 0; i < s.Len(); i++ {
			ele := s.Index(i).Int()
			formatInt := strconv.FormatInt(ele, 10)
			result = formatInt + result
		}
		return result, nil

	}
	return "", errors.New("not slice")
}
