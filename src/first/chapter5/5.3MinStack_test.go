package chapter5

import (
	"errors"
	"fmt"
	"sync"
	"testing"
)

func TestMinStack(t *testing.T) {
	stack := MinStack{}
	stack.Push(10)
	stack.Push(11)
	stack.Push(9)
	stack.Push(3)
	stack.Push(2)
	stack.Push(2)
	stack.Push(18)
	stack.Pop()
	stack.Pop()
	stack.Pop()
	stack.Pop()
	stack.Pop()
	fmt.Println(stack.GetMin())
}

//实现一个栈，该栈有出栈、入栈、取最小栈3个方法，且时间复杂度都是0
type MinStack struct {
	mainStack []int
	minStack  []int
	size      int
	lock      sync.RWMutex
}

func (stack *MinStack) Push(value int) {
	stack.lock.Lock()
	defer stack.lock.Unlock()
	root, _ := stack.Peek()
	if stack.IsEmpty() || value <= root {
		stack.minStack = append(stack.minStack, value)
	}
	stack.size++
	stack.mainStack = append(stack.mainStack, value)
}

func (stack *MinStack) Pop() (int, error) {
	stack.lock.Lock()
	defer stack.lock.Unlock()
	if stack.size == 0 {
		return 0, errors.New("empty")
	}
	v := stack.mainStack[stack.size-1]
	min := stack.minStack[len(stack.minStack)-1]
	if v == min {
		stack.minStack = stack.minStack[:len(stack.minStack)-1]
	}
	stack.mainStack = stack.mainStack[:len(stack.mainStack)-1]
	stack.size--
	return v, nil
}

func (stack *MinStack) Peek() (int, error) {
	if stack.size == 0 {
		return 0, errors.New("empty")
	}
	return stack.mainStack[stack.size-1], nil
}

func (stack *MinStack) IsEmpty() bool {
	if stack.size <= 0 {
		return true
	}
	return false
}

func (stack *MinStack) GetMin() (int, error) {
	stack.lock.RLock()
	defer stack.lock.RUnlock()
	if stack.size <= 0 {
		return 0, errors.New("empty")
	}
	return stack.minStack[len(stack.minStack)-1], nil
}
