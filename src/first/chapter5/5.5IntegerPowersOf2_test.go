package chapter5

import (
	"fmt"
	"testing"
)

//判断一个数是否为2的整数次幂
func TestIsPowerOfTwo(t *testing.T) {
	fmt.Println(IsPowerOfTwo(2))
}

func IsPowerOfTwo(value int) bool {
	return (value & (value - 1)) == 0
}
