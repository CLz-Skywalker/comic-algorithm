package chapter5

import (
	"fmt"
	"strconv"
	"strings"
	"testing"

	"comic-algorithm/src/first/utils"
)

func TestBigNumberSum(t *testing.T) {
	sum, err := BigNumberSum("9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999", "9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(sum)
}

//如何实现大整数想加
func BigNumberSum(num1, num2 string) (string, error) {
	split1 := strings.Split(num1, "")
	split2 := strings.Split(num2, "")
	listLen1 := len(split1)
	listLen2 := len(split2)

	resultLen := utils.IfNum(listLen1 > listLen2, listLen1, listLen2).(int)
	resultList := make([]int, resultLen+1)
	for i := 0; i < resultLen; i++ {
		temp := 0
		if i < listLen1 {
			num, err := strconv.Atoi(split1[i])
			if err != nil {
				return "", err
			}
			temp += num
		}
		if i < listLen2 {
			num, err := strconv.Atoi(split2[i])
			if err != nil {
				return "", err
			}
			temp += num
		}
		if temp >= 10 {
			temp -= 10
			resultList[i+1]++
		}
		resultList[i] += temp
	}
	s, err := utils.ArrayToString(resultList)
	return s, err
}
