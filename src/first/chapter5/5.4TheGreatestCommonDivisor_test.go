package chapter5

import (
	"fmt"
	"testing"
)

//最大公约数
func TestTheGreatestCommonDivisor(t *testing.T) {
	fmt.Println(gcd(10, 2))
}

func gcd(a, b int) int {
	if a == b {
		return a
	}
	if (a&1 == 0) && (b&1 == 0) {
		return gcd(a>>1, b>>1) << 1
	} else if (a&1 != 0) && (b&1 == 0) {
		return gcd(a, b>>1)
	} else if (a&1 == 0) && (b&1 != 0) {
		return gcd(a>>1, b)
	} else {
		big := a
		small := b
		if a-b < 0 {
			big = b
			small = a
		}
		return gcd(big-small, small)
	}
}
