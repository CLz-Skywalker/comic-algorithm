package chapter5

import (
	"fmt"
	"testing"

	"comic-algorithm/src/first/utils"
)

//有一个单向链表，链表中有可能出现“环”， 那么，如何用程序来判断该链表是否为有环链表呢
func TestIsCycleLinkedList(t *testing.T) {
	node1 := new(utils.LinkedList)
	node2 := new(utils.LinkedList)
	node3 := new(utils.LinkedList)
	node4 := new(utils.LinkedList)
	node5 := new(utils.LinkedList)
	node6 := new(utils.LinkedList)
	node1.Value = 1
	node2.Value = 2
	node3.Value = 3
	node4.Value = 4
	node5.Value = 5
	node6.Value = 6

	node1.Next = node2
	node2.Next = node3
	node3.Next = node4
	node4.Next = node5
	node5.Next = node6
	node6.Next = node3
	fmt.Println(isCycleLinkedList(node1))
	fmt.Println(getCycleLength(node1))
	fmt.Println(getCyclePoint(node1).Value)
}

func isCycleLinkedList(link *utils.LinkedList) bool {
	if link == nil {
		return false
	}
	a := link.NextNode()
	b := link.NextNode().NextNode()
	for a != nil && b != nil {
		if a == b {
			return true
		}
		a = a.NextNode()
		b = b.NextNode().NextNode()
	}
	return false
}

//获取环长
func getCycleLength(link *utils.LinkedList) int {
	fast := link
	slow := link
	for fast != nil && fast.Next != nil {
		fast = fast.Next.Next
		slow = slow.Next
		if slow == fast {
			length := 1
			fast = fast.Next.Next
			slow = slow.Next
			for fast != slow {
				length++
				fast = fast.Next.Next
				slow = slow.Next
			}
			return length
		}
	}
	return 0
}

//如果链表有环，求出入环节点
func getCyclePoint(link *utils.LinkedList) *utils.LinkedList {
	fast := link
	slow := link
	point := new(utils.LinkedList)
	for fast != nil && fast.Next != nil {
		fast = fast.Next.Next
		slow = slow.Next
		if fast == slow {
			slow = link
			for slow != fast {
				slow = slow.Next
				fast = fast.Next
			}
			return slow
		}
	}
	return point
}
