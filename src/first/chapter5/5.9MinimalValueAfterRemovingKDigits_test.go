package chapter5

import (
	"fmt"
	"strconv"
	"strings"
	"testing"
)

func TestRemoveDigital(t *testing.T) {
	fmt.Println(removeDigital(541270936, 3))
}

func removeDigital(num, k int) int {
	arrayStr := strconv.Itoa(num)
	array := strings.Split(arrayStr, "")
	if len(array) <= k {
		return 0
	}
	arrayNum := make([]int, 0, len(array))
	for _, v := range array {
		a, _ := strconv.Atoi(v)
		arrayNum = append(arrayNum, a)
	}
	minStack := make([]int, len(array)-k, len(array)-k)
	top := 0
	for i := 0; i < len(arrayNum); i++ {
		if top > 0 && k > 0 && minStack[top-1] > arrayNum[i] {
			top--
			k -= 1
		}
		minStack[top] = arrayNum[i]
		top++
	}
	result := 0
	for i := range minStack {
		result *= 10
		result += minStack[i]
	}
	return result
}
