package chapter5

import (
	"fmt"
	"testing"
)

//计算无序数组排序后的最大相邻差
func TestMaximumContiguousDiff(t *testing.T) {
	array := make([]int, 0, 10)
	array = append(array, 2, 1, 50, 10, 10, 200)
	fmt.Println(getMaxSortedDistanceV1(array))
}

//使用计数排序
func getMaxSortedDistanceV1(array []int) int {
	max := 0
	min := 0
	for _, value := range array {
		if value > max {
			max = value
		} else if value < min {
			min = value
		}
	}
	distance := max - min
	newArray := make([]int, distance+1)
	for _, value := range array {
		newArray[value-min]++
	}
	result := 0
	compare := 0
	for i := 0; i < len(newArray); i++ {
		if i != len(newArray)-1 && newArray[i] == 0 {
			compare++
			if compare > result {
				result = compare
			}
		} else if i == len(newArray)-1 && newArray[i] == 0 {
			compare++
			if compare > result {
				result = compare
			}
		} else {
			compare++
			if compare > result {
				result = compare
			}
			compare = 0
		}
	}
	return result
}
