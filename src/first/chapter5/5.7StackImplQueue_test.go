package chapter5

import (
	"fmt"
	"testing"

	. "comic-algorithm/src/first/utils/stack"
)

func TestStackImplQueue(t *testing.T) {
	queue := StackImplQueueStruct{}
	queue.Push(1)
	queue.Push(2)
	queue.Push(3)
	fmt.Println(queue.Pop())
	queue.Push(4)
	fmt.Println(queue.Pop())
	queue.Push(5)
	fmt.Println(queue.Pop())
	queue.Push(6)
	fmt.Println(queue.Pop())
	fmt.Println(queue.Pop())
	fmt.Println(queue.Pop())
}

type StackImplQueueStruct struct {
	PushStack *ArrayStack
	PopStack  *ArrayStack
}

//入队
func (stack *StackImplQueueStruct) Push(value interface{}) {
	if stack.PushStack == nil {
		stack.PushStack = new(ArrayStack)
	}
	stack.PushStack.Push(value)
}

//出队
func (stack *StackImplQueueStruct) Pop() interface{} {
	if stack.PopStack == nil {
		stack.PopStack = new(ArrayStack)
	}
	if !stack.PopStack.IsEmpty() {
		value, err := stack.PopStack.Pop()
		if err != nil {
			panic(err)
		}
		return value
	}
	for !stack.PushStack.IsEmpty() {
		v, err := stack.PushStack.Pop()
		if err != nil {
			panic(err)
		}
		stack.PopStack.Push(v)
	}
	value, err := stack.PopStack.Pop()
	if err != nil {
		panic(err)
	}
	return value
}
