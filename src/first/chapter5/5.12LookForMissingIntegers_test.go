/**
  @author: ClzSkywalker
  @since: 2021-08-01
  @desc: 寻找缺失的整数
**/
package chapter5

import (
	"fmt"
	"testing"
)

func TestLookForMissingInters(t *testing.T) {
	list := []int{10, 10, 2, 2, 9, 80}
	l := LookForMissingInters(list)
	if l == nil {
		fmt.Println("nil")
		return
	}
	fmt.Println(l)
}

/**
 * @Description: 寻找缺失的两个不同整数
 * @param list
 * @return []int
 */
func LookForMissingInters(list []int) []int {
	result := make([]int, 2)
	xorResult := 0
	for _, v := range list {
		xorResult ^= v
	}
	if xorResult == 0 {
		return nil
	}
	separator := 1
	for (xorResult & separator) == 0 {
		separator <<= 1
	}
	for _, v := range list {
		if v&separator == 0 {
			result[0] ^= v
		} else {
			result[1] ^= v
		}
	}
	return result
}
