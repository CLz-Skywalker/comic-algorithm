package chapter5

import (
	"fmt"
	"strconv"
	"strings"
	"testing"
)

func TestNextNumInTheFullArray(t *testing.T) {
	a := 12345
	for i := 0; i < 10; i++ {
		a = NextNumInTheFullArray(a)
		fmt.Println(a)
	}
	fmt.Println(NextNumInTheFullArray(0))
}

func NextNumInTheFullArray(num int) int {

	s := strconv.Itoa(num)
	split := strings.Split(s, "")
	list := make([]int, 0, len(split))
	for _, v := range split {
		num, _ := strconv.Atoi(v)
		list = append(list, num)
	}
	index := findTransferPoint(list)
	if index == -1 {
		return num
	}
	exchangeHead(list, index)
	reverse(list, index)
	var result int
	for _, v := range list {
		result *= 10
		result += v
	}
	return result
}

//寻找前一个数大于后一个数
func findTransferPoint(list []int) int {
	if len(list) <= 1 {
		return -1
	}
	for i := len(list) - 1; i > 0; i-- {
		if list[i] > list[i-1] {
			return i
		}
	}
	return -1
}

//让逆序区域的前一位与逆序区域中的大于它的最小数进行交换
func exchangeHead(list []int, index int) {
	head := list[index-1]
	for i := len(list) - 1; i > 0; i-- {
		if head < list[i] {
			list[index-1] = list[i]
			list[i] = head
			break
		}
	}
}

//让逆序区域顺序化
func reverse(list []int, index int) {
	for i := len(list) - 1; i > index; i-- {
		temp := list[index]
		list[index] = list[i]
		list[i] = temp
		index++
	}
}
