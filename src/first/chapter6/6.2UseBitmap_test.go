package chapter6

import (
	"errors"
	"fmt"
	"testing"
)

/**
 * @Description: 6.2 bitmap巧用
 */
type MyBitmap struct {
	Worlds []uint64
	length int
}

/**
 * @Description: 初始化
 * @return *MyBitmap
 */
func NewBitMap() *MyBitmap {
	return &MyBitmap{}
}

/**
 * @Description: 判断bitmap某一位的状态
 * @receiver m
 * @param num
 * @return bool
 * @return error
 */
func (m *MyBitmap) GetBit(num int) (bool, error) {
	if num < 0 {
		return false, errors.New("num<0")
	}
	word, bit := num/64, num%64
	return word < len(m.Worlds) && (m.Worlds[word]&(1<<bit)) != 0, nil
}

/**
 * @Description: 把bitmap某一位设置为true
 * @receiver m
 * @param bitIndex
 * @return error
 */
func (m *MyBitmap) SetBit(num int) error {
	if num < 0 {
		return errors.New("num<0")
	}
	word, bit := num/64, num%64
	for word >= len(m.Worlds) {
		m.Worlds = append(m.Worlds, 0)
	}
	if m.Worlds[word]&(1<<bit) == 0 {
		m.Worlds[word] |= 1 << bit
		m.length++
	}
	return nil
}

func TestBitmap(t *testing.T) {
	bitMap := NewBitMap()
	err := bitMap.SetBit(1)
	if err != nil {
		fmt.Println(err)
		return
	}
	err = bitMap.SetBit(2)
	if err != nil {
		fmt.Println(err)
		return
	}
	has, _ := bitMap.GetBit(1)
	fmt.Println(has)
	has, _ = bitMap.GetBit(2)
	fmt.Println(has)
}
