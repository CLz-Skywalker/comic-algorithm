package chapter6

import (
	"container/list"
	"fmt"
	"testing"
)

type Key interface{}

type entry struct {
	Key   Key
	value interface{}
}

type LRUCacheStruct struct {
	MaxEntries int
	OnEvicted  func(key Key, value interface{})
	ll         *list.List
	cache      map[interface{}]*list.Element
}

func NewLRUCacheStruct(maxEntries int) *LRUCacheStruct {
	return &LRUCacheStruct{
		MaxEntries: maxEntries,
		ll:         list.New(),
		cache:      make(map[interface{}]*list.Element),
	}
}

/**
 * @Description:添加
 * @receiver l
 * @param key
 * @param value
 */
func (l *LRUCacheStruct) Add(key Key, value interface{}) {
	if l.cache == nil {
		l.cache = make(map[interface{}]*list.Element)
		l.ll = list.New()
	}
	if e, ok := l.cache[key]; ok {
		l.ll.PushFront(e)
		e.Value.(*entry).value = value
		return
	}
	ele := l.ll.PushFront(&entry{
		Key:   key,
		value: value,
	})
	l.cache[key] = ele
	if l.MaxEntries != 0 && l.ll.Len() > l.MaxEntries {
		l.RemoveOldest()
	}
}

/**
 * @Description:根据key获取value
 * @receiver l
 * @param key
 * @return value
 * @return ok
 */
func (l *LRUCacheStruct) Get(key Key) (value interface{}, ok bool) {
	if l.cache == nil {
		return
	}
	if element, hit := l.cache[key]; hit {
		value = element.Value.(*entry).value
		ok = hit
		l.ll.MoveToFront(element)
		return
	}
	return
}

/**
 * @Description:从缓存中删除一个 KV
 * @receiver l
 * @param key
 */
func (l *LRUCacheStruct) Remove(key Key) {
	if l.cache == nil {
		return
	}
	if ele, ok := l.cache[key]; ok {
		l.removeElement(ele)
	}
}

/**
 * @Description:从缓存中删除最久未被访问的数据
 * @receiver l
 */
func (l *LRUCacheStruct) RemoveOldest() {
	if l.cache == nil {
		return
	}
	ele := l.ll.Back()
	if ele != nil {
		l.removeElement(ele)
	}
}

/**
 * @Description:从缓存中删除一个元素，供内部使用
 * @receiver l
 * @param e
 */
func (l *LRUCacheStruct) removeElement(e *list.Element) {
	l.ll.Remove(e)
	kv := e.Value.(*entry)
	delete(l.cache, kv.Key)
	if l.OnEvicted != nil {
		l.OnEvicted(kv.Key, kv.value)
	}
}

/**
 * @Description:获取缓存长度
 * @receiver l
 * @return int
 */
func (l *LRUCacheStruct) Len() int {
	if l.cache == nil {
		return 0
	}
	return l.ll.Len()
}

/**
 * @Description:清空缓存
 * @receiver l
 */
func (l *LRUCacheStruct) Clear() {
	if l.OnEvicted != nil {
		for _, e := range l.cache {
			kv := e.Value.(*entry)
			l.OnEvicted(kv.Key, kv.value)
		}
	}
	l.ll = nil
	l.cache = nil
}

func TestLRUCacheStruct(t *testing.T) {
	cache := NewLRUCacheStruct(2)
	cache.Add("bill", 20)
	cache.Add("dable", 19)
	v, ok := cache.Get("bill")
	if ok {
		fmt.Printf("bill's age is %v\n", v)
	}
	cache.Add("cat", "18")

	fmt.Printf("cache length is %d\n", cache.Len())
	_, ok = cache.Get("dable")
	if !ok {
		fmt.Printf("dable was evicted out\n")
	}
}
